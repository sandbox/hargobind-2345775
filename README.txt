INTRODUCTION
------------

This is a set of Context condition plugins that extend the context page
conditions allowing you to specify options for ctools pages or panels.


INSTALLATION
------------

 * Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7 for further information.

 * Clear your cache, see https://www.drupal.org/node/42055 for further information.

CONFIGURATION
-------------

No special configuration necessary beyond the Context condition UI.

CONTACT
-------

Current maintainers:
* Hargobind Khalsa (hargobind) - https://www.drupal.org/user/216765
