<?php

/**
 * Conditions for ctools pages and panels.
 */
class context_ctools_conditions_condition_ctools_pages extends context_condition {
  function condition_values() {
    return array(
      t('No'),
      t('Yes'),
    );
  }

  function condition_form($context = NULL) {
    $form = parent::condition_form($context);
    $form['#type'] = 'select';
    $values = $this->fetch_from_context($context, 'values');
    $form['#default_value'] = $values[0];
    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    // Since our condition form is a radio and it returns a single value,
    // it needs to be converted into an array for storage.
    return array($values);
  }

  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');

    $items = array(
      'ctools_set_no_blocks' => array(
        '#title' => t('Value of "disable Drupal blocks/regions"'),
        '#type' => 'select',
        '#options' => array(
          -1 => t('Any value'),
          0 => t('No'),
          1 => t('Yes'),
        ),
        '#description' => t('Select a matching value for this setting that was selected on the General tab of the page variant.'),
        '#default_value' => isset($defaults['ctools_set_no_blocks']) ? $defaults['ctools_set_no_blocks'] : -1,
      ),
      'operator' => array(
        '#title' => t('Operator'),
        '#type' => 'select',
        '#options' => array(
          'and' => t('And'),
          'or' => t('Or'),
        ),
        '#description' => t('Choose if the conditions above are combined or treated separately.'),
        '#default_value' => isset($defaults['operator']) ? $defaults['operator'] : '',
      ),
    );

    return $items;
  }

  function execute() {
    foreach ($this->get_contexts() as $context) {
      // Condition settings.
      if (!isset($context->conditions['ctools_pages'])) {
        $context->conditions['ctools_pages'] = array(
          'values' => array(0),
          'options' => array(
            'ctools_set_no_blocks' => -1,
            'operator' => '',
          ),
        );
      }
      $setting_react_on_page = $context->conditions['ctools_pages']['values'][0];
      $setting_blocks_regions = $context->conditions['ctools_pages']['options']['ctools_set_no_blocks'];
      $operator = $context->conditions['ctools_pages']['options']['operator'];

      // Test the conditions.
      $router_item = menu_get_item();
      $match_ctools_page = $router_item['page_callback'] == 'page_manager_page_execute';
      $match_panels_blocks = $setting_blocks_regions != drupal_static('ctools_set_no_blocks', TRUE);
      
      // Compare the conditions.
      $condition_met = $setting_react_on_page ? $match_ctools_page : !$match_ctools_page;
      if ($setting_blocks_regions >= 0) {
        if ($operator == 'and')
          $condition_met &= $match_panels_blocks;
        else
          $condition_met |= $match_panels_blocks;
      }

      if ($condition_met) {
        $this->condition_met($context);
      }
    }
  }
}
